package com.s3group.nilgiri.dao;

import java.util.List;

import com.s3group.nilgiri.entity.Counter;

public interface CounterDAO {
	
	public List<Counter> findAllCounters();
	
	public List<Counter> saveCounter(Counter counter);
	
	public List<Counter> updateCounter(Counter counter);
	
	public void deleteCounter(Long id);
	
	public Counter findByCounterId(Long id);

}