package com.s3group.nilgiri.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.s3group.nilgiri.entity.Location;


@Transactional
@Repository
public class LocationDAOImpl implements LocationDAO {
	
public static final Logger logger = LoggerFactory.getLogger(LocationDAOImpl.class);
	
	@PersistenceContext	
	private EntityManager entityManager;	
	
	@Override
	public Location findByLocationId(Long id) {
		return entityManager.find(Location.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Location> findAllLocations(){		
		String hql = "FROM Location as lc ORDER BY lc.locationId";
		return (List<Location>) entityManager.createQuery(hql).getResultList();		
	}
	
	@Override
	public List<Location> saveLocation(Location location) {		
		List<Location> locations = new ArrayList<Location>();
		try {
			logger.info("Location = "+location.getCompany());
			entityManager.persist(location);	
			logger.info("Persisted");
			locations = findAllLocations();
			logger.info("LocationSize = "+locations.size());
		}catch(Exception e) {e.printStackTrace();}
		
		return locations;						
	}
	
	@Override
	public List<Location> updateLocation(Location location) {
		List<Location> locations = new ArrayList<Location>();
		Location lc = findByLocationId(location.getLocationId());
		if(lc == null) {
			logger.error("Unable to update. Location with id {} not found.", location.getLocationId());
		}else {			
			entityManager.merge(location);
			locations = findAllLocations();
		}
		return locations;
	}
	
	@Override
	public void deleteLocation(Long id) {
		entityManager.remove(findByLocationId(id));
	}	
	
}
