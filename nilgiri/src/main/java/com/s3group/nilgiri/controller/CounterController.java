package com.s3group.nilgiri.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.s3group.nilgiri.dto.CounterDTO;
import com.s3group.nilgiri.dto.ResponseDTO;
import com.s3group.nilgiri.service.CounterService;

@RestController
@RequestMapping("/api")
public class CounterController {
	
	public static final Logger logger = LoggerFactory.getLogger(CounterController.class);
	
	@Autowired
	private CounterService counterService; 
	
	@RequestMapping(value = "/counter/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CounterDTO>> listAllCounters() {
		logger.info("Getting All Counters.");			
		List<CounterDTO> counterList = counterService.findAllCounters();
		if (counterList.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);			
		}
		return new ResponseEntity<List<CounterDTO>>(counterList, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/counter/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> createCounter(@RequestBody @Valid CounterDTO counter) {		
		logger.info("Creating Counter : {}", counter);		
		List<CounterDTO> counters = counterService.saveCounter(counter);
		ResponseDTO res = new ResponseDTO();
		if (counters.isEmpty())
		{
		res.setError_code("1");
		res.setStatus("EMPTY");
		}
		else 
		{
			res.setError_code("0");
			res.setStatus("CREATED");
			res.setData(counters);
		}
		
		return new ResponseEntity<ResponseDTO>(res, HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value = "/counter/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> updateCounter(@RequestBody CounterDTO counter) {
		logger.info("Updating Counter with id {}", counter.getId());
		List<CounterDTO> counters = counterService.updateCounter(counter);
		ResponseDTO res = new ResponseDTO();
		if (counters.isEmpty())
		{
		res.setError_code("1");
		res.setStatus("EMPTY");
		}
		else 
		{
			res.setError_code("0");
			res.setStatus("UPDATED");
			res.setData(counters);
		}
		
		return new ResponseEntity<ResponseDTO>(res, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/counter/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteCounter(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Counter with id {}", id);
		counterService.deleteCounter(id);
		return new ResponseEntity<String>("DELETED", HttpStatus.OK);
	}
	
}
