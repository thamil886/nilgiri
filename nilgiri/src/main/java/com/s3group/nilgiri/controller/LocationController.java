package com.s3group.nilgiri.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.s3group.nilgiri.dto.LocationDTO;
import com.s3group.nilgiri.service.LocationService;

@RestController
@RequestMapping("/api")
public class LocationController {
	
public static final Logger logger = LoggerFactory.getLogger(LocationController.class);
	
	@Autowired
	private LocationService locationService; 
	
	@RequestMapping(value = "/location/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LocationDTO>> listAllLocations() {
		logger.info("Getting All Locations.");			
		List<LocationDTO> locationList = locationService.findAllLocations();
		if (locationList.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);			
		}
		return new ResponseEntity<List<LocationDTO>>(locationList, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/location/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LocationDTO>> createLocation(@RequestBody LocationDTO location) {		
		logger.info("Creating Location : {}", location);		
		List<LocationDTO> locations = locationService.saveLocation(location);
		return new ResponseEntity<List<LocationDTO>>(locations, HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value = "/location/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LocationDTO>> updateLocation(@RequestBody LocationDTO location) {
		logger.info("Updating Location with id {}", location.getId());
		List<LocationDTO> locations = locationService.updateLocation(location);
		return new ResponseEntity<List<LocationDTO>>(locations, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/location/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteLocation(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Location with id {}", id);
		locationService.deleteLocation(id);
		return new ResponseEntity<String>("DELETED", HttpStatus.OK);
	}

}
