package com.s3group.nilgiri.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.s3group.nilgiri.dto.ResponseDTO;
import com.s3group.nilgiri.dto.WarehouseDTO;

import com.s3group.nilgiri.service.WarehouseService;

@RestController
@RequestMapping("/api")

public class WarehouseController {
	
		public static final Logger logger = LoggerFactory.getLogger(WarehouseController.class);
		
		@Autowired
		private WarehouseService warehouseService; 
		
		@RequestMapping(value = "/warehouse/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<List<WarehouseDTO>> listAllWarehouses() {
			logger.info("Getting All Warehouses.");			
			List<WarehouseDTO> warehouseList = warehouseService.findAllWarehouses();
			if (warehouseList.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);			
			}
			return new ResponseEntity<List<WarehouseDTO>>(warehouseList, HttpStatus.OK);
		}
		
		
		@RequestMapping(value = "/warehouse/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<ResponseDTO> createWarehouse(@RequestBody @Valid WarehouseDTO warehouse) {		
			logger.info("Creating Warehouse : {}", warehouse);		
			List<WarehouseDTO> warehouses = warehouseService.saveWarehouse(warehouse);
			ResponseDTO res = new ResponseDTO();
			if (warehouses.isEmpty())
			{
			res.setError_code("1");
			res.setStatus("EMPTY");
			}
			else 
			{
				res.setError_code("0");
				res.setStatus("CREATED");
				res.setData(warehouses);
			}
			
			return new ResponseEntity<ResponseDTO>(res, HttpStatus.CREATED);
		}
		
		
		@RequestMapping(value = "/warehouse/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<ResponseDTO> updateWarehouse(@RequestBody WarehouseDTO warehouse) {
			logger.info("Updating Warehouse with id {}", warehouse.getWarehouseId());
			List<WarehouseDTO> warehouses = warehouseService.updateWarehouse(warehouse);
			ResponseDTO res = new ResponseDTO();
			if (warehouses.isEmpty())
			{
			res.setError_code("1");
			res.setStatus("EMPTY");
			}
			else 
			{
				res.setError_code("0");
				res.setStatus("UPDATED");
				res.setData(warehouses);
			}
			
			return new ResponseEntity<ResponseDTO>(res, HttpStatus.OK);
		}
		
		
		@RequestMapping(value = "/warehouse/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<String> deleteWarehouse(@PathVariable("id") long warehouseId) {
			logger.info("Fetching & Deleting warehouse with id {}", warehouseId);
			warehouseService.deleteWarehouse(warehouseId);
			return new ResponseEntity<String>("DELETED", HttpStatus.OK);
		}
		
	}


