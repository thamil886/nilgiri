package com.s3group.nilgiri.dto;

public class CounterDTO {
	
	private Long id;
	private String company;
	private String division;
	private String warehouse;
	private String recordType;
	private String description;
	private String prefix;
	private double preLength;
	private double startNumber;
	private double endNumber;
	private double currentNumber;
	private double currentNumberLength;
	private double incrementNumber;
	private String checkDigitType;
	private double checkDigitLength;
	private String resetFlag;
	private String checkDigitFlag;
	private String systemExpansion;
	private String customerExpansion;
	
	public CounterDTO() {}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public double getPreLength() {
		return preLength;
	}
	public void setPreLength(double preLength) {
		this.preLength = preLength;
	}
	public double getStartNumber() {
		return startNumber;
	}
	public void setStartNumber(double startNumber) {
		this.startNumber = startNumber;
	}
	public double getEndNumber() {
		return endNumber;
	}
	public void setEndNumber(double endNumber) {
		this.endNumber = endNumber;
	}
	public double getCurrentNumber() {
		return currentNumber;
	}
	public void setCurrentNumber(double currentNumber) {
		this.currentNumber = currentNumber;
	}
	public double getCurrentNumberLength() {
		return currentNumberLength;
	}
	public void setCurrentNumberLength(double currentNumberLength) {
		this.currentNumberLength = currentNumberLength;
	}
	public double getIncrementNumber() {
		return incrementNumber;
	}
	public void setIncrementNumber(double incrementNumber) {
		this.incrementNumber = incrementNumber;
	}
	public String getCheckDigitType() {
		return checkDigitType;
	}
	public void setCheckDigitType(String checkDigitType) {
		this.checkDigitType = checkDigitType;
	}
	public double getCheckDigitLength() {
		return checkDigitLength;
	}
	public void setCheckDigitLength(double checkDigitLength) {
		this.checkDigitLength = checkDigitLength;
	}
	
	public String getResetFlag() {
		return resetFlag;
	}
	public void setResetFlag(String resetFlag) {
		this.resetFlag = resetFlag;
	}
	public String getCheckDigitFlag() {
		return checkDigitFlag;
	}
	public void setCheckDigitFlag(String checkDigitFlag) {
		this.checkDigitFlag = checkDigitFlag;
	}

	public String getSystemExpansion() {
		return systemExpansion;
	}
	public void setSystemExpansion(String systemExpansion) {
		this.systemExpansion = systemExpansion;
	}
	public String getCustomerExpansion() {
		return customerExpansion;
	}
	public void setCustomerExpansion(String customerExpansion) {
		this.customerExpansion = customerExpansion;
	}
	
	
}
