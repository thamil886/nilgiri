package com.s3group.nilgiri.dto;

public class ResponseDTO {
	private String Error_code;
	private String status;
	private Object data;
	
	public String getError_code()
	{
		return Error_code;
	}
	
	public void setError_code(String Error_code)
	{
		this.Error_code= Error_code;
	}
	
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status= status;
	}
	public Object getData()
	{
		return data;
	}
	public void setData(Object data)
	{
		this.data=data;
	}
}
