package com.s3group.nilgiri.dto;

public class LocationAtr1DTO {

	private Long id;
	//private int locationId;
	private String company;
	private String division;
	private String warehouse;
	private String locationType;
	private String locnBarcode;
	private int height;
	private int length;
	private int width;
	private int maxInventoryCases;
	private int minInventoryCases;
	private int actualCases;
	private int maxUnits;
	private int minInventoryUnits;
	private int actualUnits;
	private int maxVolume;
	private int minVolume;
	private int actualVolume;
	private int maxWeight;
	private int minWeight;
	private int actualWeight;
	
	public LocationAtr1DTO() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}*/

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public String getLocnBarcode() {
		return locnBarcode;
	}

	public void setLocnBarcode(String locnBarcode) {
		this.locnBarcode = locnBarcode;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getMaxInventoryCases() {
		return maxInventoryCases;
	}

	public void setMaxInventoryCases(int maxInventoryCases) {
		this.maxInventoryCases = maxInventoryCases;
	}

	public int getMinInventoryCases() {
		return minInventoryCases;
	}

	public void setMinInventoryCases(int minInventoryCases) {
		this.minInventoryCases = minInventoryCases;
	}

	public int getActualCases() {
		return actualCases;
	}

	public void setActualCases(int actualCases) {
		this.actualCases = actualCases;
	}

	public int getMaxUnits() {
		return maxUnits;
	}

	public void setMaxUnits(int maxUnits) {
		this.maxUnits = maxUnits;
	}

	public int getMinInventoryUnits() {
		return minInventoryUnits;
	}

	public void setMinInventoryUnits(int minInventoryUnits) {
		this.minInventoryUnits = minInventoryUnits;
	}

	public int getActualUnits() {
		return actualUnits;
	}

	public void setActualUnits(int actualUnits) {
		this.actualUnits = actualUnits;
	}

	public int getMaxVolume() {
		return maxVolume;
	}

	public void setMaxVolume(int maxVolume) {
		this.maxVolume = maxVolume;
	}

	public int getMinVolume() {
		return minVolume;
	}

	public void setMinVolume(int minVolume) {
		this.minVolume = minVolume;
	}

	public int getActualVolume() {
		return actualVolume;
	}

	public void setActualVolume(int actualVolume) {
		this.actualVolume = actualVolume;
	}

	public int getMaxWeight() {
		return maxWeight;
	}

	public void setMaxWeight(int maxWeight) {
		this.maxWeight = maxWeight;
	}

	public int getMinWeight() {
		return minWeight;
	}

	public void setMinWeight(int minWeight) {
		this.minWeight = minWeight;
	}

	public int getActualWeight() {
		return actualWeight;
	}

	public void setActualWeight(int actualWeight) {
		this.actualWeight = actualWeight;
	}	
	
	
}
