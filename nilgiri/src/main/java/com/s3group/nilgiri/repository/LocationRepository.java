package com.s3group.nilgiri.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.s3group.nilgiri.entity.Location;

@Repository
public interface LocationRepository  extends PagingAndSortingRepository<Location,Long> {
	
	Location findByLocationId(Long id);

}
