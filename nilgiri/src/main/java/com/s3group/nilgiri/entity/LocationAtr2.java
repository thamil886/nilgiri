package com.s3group.nilgiri.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="LOCATION_ATR2")
public class LocationAtr2 implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="LOCATION_ATRID")
	private Long id;	
	
	@Column(name="COMPANY")
	private String company;
	
	@Column(name="DIVISION")
	private String division;
	
	@Column(name="WAREHOUSE")
	private String warehouse;
	
	@Column(name="LOCATION_TYPE")
	private String locationType;
	
	@Column(name="LOCN_BARCODE")
	private String locnBarcode;
	
	@Column(name="ITEM_BARCODE")
	private String itemBarcode;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="LOCATION_ID", nullable=false)		
	private Location location;
	
	public LocationAtr2() {		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public String getLocnBarcode() {
		return locnBarcode;
	}

	public void setLocnBarcode(String locnBarcode) {
		this.locnBarcode = locnBarcode;
	}

	public String getItemBarcode() {
		return itemBarcode;
	}

	public void setItemBarcode(String itemBarcode) {
		this.itemBarcode = itemBarcode;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
}
