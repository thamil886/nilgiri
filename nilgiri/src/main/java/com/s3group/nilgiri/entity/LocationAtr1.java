package com.s3group.nilgiri.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="LOCATION_ATR1")
public class LocationAtr1 implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="LOCATION_ATRID")
	private Long id;
		
	@Column(name="COMPANY")
	private String company;
	
	@Column(name="DIVISION")
	private String division;
	
	@Column(name="WAREHOUSE")
	private String warehouse;
	
	@Column(name="LOCATION_TYPE")
	private String locationType;
	
	@Column(name="LOCN_BARCODE")
	private String locnBarcode;
	
	@Column(name="HEIGHT")
	private int height;
	
	@Column(name="LENGTH")
	private int length;
	
	@Column(name="WIDTH")
	private int width;
	
	@Column(name="MAX_INVEN_CASES")
	private int maxInventoryCases;
	
	@Column(name="MIN_INVEN_CASES")
	private int minInventoryCases;
	
	@Column(name="ACTAUL_CASES")
	private int actualCases;
	
	@Column(name="MAX_UNITS")
	private int maxUnits;
	
	@Column(name="MIN_INVEN_UNITS")
	private int minInventoryUnits;
	
	@Column(name="ACTUAL_UNITS")
	private int actualUnits;
	
	@Column(name="MAX_VOLUME")
	private int maxVolume;
	
	@Column(name="MIN_VOLUME")
	private int minVolume;
	
	@Column(name="ACTUAL_VOLUME")
	private int actualVolume;
	
	@Column(name="MAX_WEIGHT")
	private int maxWeight;
	
	@Column(name="MIN_Weight")
	private int minWeight;
	
	@Column(name="ACTUAL_WEIGHTE")
	private int actualWeight;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="LOCATION_ID", nullable=false)		
	private Location location;
	
	public LocationAtr1() {		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public String getLocnBarcode() {
		return locnBarcode;
	}

	public void setLocnBarcode(String locnBarcode) {
		this.locnBarcode = locnBarcode;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getMaxInventoryCases() {
		return maxInventoryCases;
	}

	public void setMaxInventoryCases(int maxInventoryCases) {
		this.maxInventoryCases = maxInventoryCases;
	}

	public int getMinInventoryCases() {
		return minInventoryCases;
	}

	public void setMinInventoryCases(int minInventoryCases) {
		this.minInventoryCases = minInventoryCases;
	}

	public int getActualCases() {
		return actualCases;
	}

	public void setActualCases(int actualCases) {
		this.actualCases = actualCases;
	}

	public int getMaxUnits() {
		return maxUnits;
	}

	public void setMaxUnits(int maxUnits) {
		this.maxUnits = maxUnits;
	}

	public int getMinInventoryUnits() {
		return minInventoryUnits;
	}

	public void setMinInventoryUnits(int minInventoryUnits) {
		this.minInventoryUnits = minInventoryUnits;
	}

	public int getActualUnits() {
		return actualUnits;
	}

	public void setActualUnits(int actualUnits) {
		this.actualUnits = actualUnits;
	}

	public int getMaxVolume() {
		return maxVolume;
	}

	public void setMaxVolume(int maxVolume) {
		this.maxVolume = maxVolume;
	}

	public int getMinVolume() {
		return minVolume;
	}

	public void setMinVolume(int minVolume) {
		this.minVolume = minVolume;
	}

	public int getActualVolume() {
		return actualVolume;
	}

	public void setActualVolume(int actualVolume) {
		this.actualVolume = actualVolume;
	}

	public int getMaxWeight() {
		return maxWeight;
	}

	public void setMaxWeight(int maxWeight) {
		this.maxWeight = maxWeight;
	}

	public int getMinWeight() {
		return minWeight;
	}

	public void setMinWeight(int minWeight) {
		this.minWeight = minWeight;
	}

	public int getActualWeight() {
		return actualWeight;
	}

	public void setActualWeight(int actualWeight) {
		this.actualWeight = actualWeight;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

		
}
