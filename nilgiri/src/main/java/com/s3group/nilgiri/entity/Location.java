package com.s3group.nilgiri.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="LOCATION")
public class Location implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="LOCATION_ID")
	private Long locationId;
	
	@Column(name="COMPANY")
	private String company;
	
	@Column(name="DIVISION")
	private String division;
	
	@Column(name="WAREHOUSE")
	private String warehouse;
	
	@Column(name="LOCATION_TYPE")
	private String locationType; 
	
	@Column(name="AREA")
	private String area;
	
	@Column(name="ZONE")
	private String zone;
	
	@Column(name="AISLE")
	private String aisle;
	
	@Column(name="BAY")
	private String bay;
	
	@Column(name="LEVEL")
	private String level;
	
	@Column(name="POSITION")
	private String position;
	
	@Column(name="SEQ_NUMBER")
	private int sequenceNumber;
	
	@Column(name="LOCN_BARCODE")
	private String locnBarcode;
	
	@Column(name="LOCN_SIZETYPE")
	private String locnSizeType;
	
	@Column(name="LOCN_PRODTYPE")
	private String locnProductType;
	
	@Column(name="PUTAWAY_ZONE")
	private String putawayZone;
	
	@Column(name="LOCN_LOCKED")
	private char locnLocked;
	
	@Column(name="INVENT_LOCKCODE")
	private String invenLockCode;
	
	@Column(name="STATUS_FLAG")
	private String statusFlag;
	
	@OneToMany(mappedBy = "location", cascade = CascadeType.ALL, orphanRemoval = true)
	private List <LocationAtr1> locations1 = new ArrayList<>();
	
	@OneToMany(mappedBy = "location", cascade = CascadeType.ALL, orphanRemoval = true)
	private List <LocationAtr2> locations2 = new ArrayList<>();
	
	public Location() {		
	}
	
	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getAisle() {
		return aisle;
	}

	public void setAisle(String aisle) {
		this.aisle = aisle;
	}

	public String getBay() {
		return bay;
	}

	public void setBay(String bay) {
		this.bay = bay;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getLocnBarcode() {
		return locnBarcode;
	}

	public void setLocnBarcode(String locnBarcode) {
		this.locnBarcode = locnBarcode;
	}

	public String getLocnSizeType() {
		return locnSizeType;
	}

	public void setLocnSizeType(String locnSizeType) {
		this.locnSizeType = locnSizeType;
	}

	public String getLocnProductType() {
		return locnProductType;
	}

	public void setLocnProductType(String locnProductType) {
		this.locnProductType = locnProductType;
	}

	public String getPutawayZone() {
		return putawayZone;
	}

	public void setPutawayZone(String putawayZone) {
		this.putawayZone = putawayZone;
	}

	public char getLocnLocked() {
		return locnLocked;
	}

	public void setLocnLocked(char locnLocked) {
		this.locnLocked = locnLocked;
	}

	public String getInvenLockCode() {
		return invenLockCode;
	}

	public void setInvenLockCode(String invenLockCode) {
		this.invenLockCode = invenLockCode;
	}

	public String getStatusFlag() {
		return statusFlag;
	}

	public void setStatusFlag(String statusFlag) {
		this.statusFlag = statusFlag;
	}

	public List<LocationAtr1> getLocations1() {
		return locations1;
	}

	public void setLocations1(List<LocationAtr1> locations1) {
		this.locations1 = locations1;
	}

	public List<LocationAtr2> getLocations2() {
		return locations2;
	}

	public void setLocations2(List<LocationAtr2> locations2) {
		this.locations2 = locations2;
	}
		
}
