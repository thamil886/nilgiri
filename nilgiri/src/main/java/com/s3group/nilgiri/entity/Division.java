package com.s3group.nilgiri.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/*import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;*/

@Entity
@Table(name="DIVISION")
public class Division implements Serializable{
	
	 @ManyToOne(fetch = FetchType.LAZY)
	 @JoinColumn(name = "COMPANY_ID", nullable=false)
	 	private Company company;
	 
	 public Division () {
		 
	 }
	 
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DIVISION_ID")
	private Long divisionId;
	
	/*@Column(name="COMPANY_ID")
	private Long companyId;*/
	
	@Column(name="DIVN_DIVISION")
	private String divndivision;
	
	@Column(name="DIVN_DESC")
	private String divndesc;
	
	@Column(name="DIVN_ADDRESS1")
	private String divnaddress1;
	
	@Column(name="DIVN_ADDRESS2")
	private String divnaddress2;
	
	@Column(name="DIVN_CITY")
	private String divncity;
	
	@Column(name="DIVN_STATE")
	private String divnstate;
	
	@Column(name="DIVN_ZIP")
	private String divnzip;
	
	@Column(name="DIVN_COUNTRY")
	private String divncountry;
	
	@Column(name="DIVN_CONTACT_NAME")
	private String divncontactname;
	
	@Column(name="DIVN_CONTACT_NUMBER")
	private String divncontactnumber;
	
	/*public Long getCompanyId() {
		return companyId;
	}
	
	public void setCompanyId(Long companyId) {
		this.companyId=companyId;
	}*/
	
	public Long getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Long divisionId) {
		this.divisionId = divisionId;
	}
	
	public String getDivnDivision() {
		return divndivision;
	}

	public void setDivnDivision(String divndivision) {
		this.divndivision = divndivision;
	}
	
	public String getDivnDesc() {
		return divndesc;
	}

	public void setDivnDesc(String divndesc) {
		this.divndesc = divndesc;
	}

	public String getDivnAddress1() {
		return divnaddress1;
	}

	public void setDivnAddress1(String divnaddress1) {
		this.divnaddress1 = divnaddress1;
	}
	
	public String getDivnAddress2() {
		return divnaddress2;
	}

	public void setDivnAddress2(String divnaddress2) {
		this.divnaddress2 = divnaddress2;
	}
	
	public String getDivnCity() {
		return divncity;
	}

	public void setDivnCity(String divncity) {
		this.divncity = divncity;
	}
	
	public String getDivnState() {
		return divnstate;
	}

	public void setDivnState(String divnstate) {
		this.divnstate = divnstate;
	}
	
	public String getDivnZip() {
		return divnzip;
	}

	public void setDivnZip(String divnzip) {
		this.divnzip = divnzip;
	}
	
	public String getDivnCountry() {
		return divncountry;
	}

	public void setDivnCountry(String divncountry) {
		this.divncountry = divncountry;
	}
	
	public String getDivnContactname() {
		return divncontactname;
	}

	public void setDivnContactname(String divncontactname) {
		this.divncontactname = divncontactname;
	}
	
	public String getDivnContactnumber() {
		return divncontactnumber;
	}

	public void setDivnContactnumber(String divncontactnumber) {
		this.divncontactnumber = divncontactnumber;
	}
	
	/*public String getDivncreatetimestamp() {
		return divncreatetimestamp;
	}

	public void setDivncreatetimestamp(String divncreatetimestamp) {
		this.divncreatetimestamp = divncreatetimestamp;
	}
	
	public String getDivnchangetimestamp() {
		return divnchangetimestamp;
	}

	public void setDivnchangetimestamp(String divnchangetimestamp) {
		this.divnchangetimestamp = divnchangetimestamp;
	}
	
	public String getDivncreateuser() {
		return divncreateuser;
	}

	public void setDivncreateuser(String divncreateuser) {
		this.divncreateuser = divncreateuser;
	}
	
	public String getDivnchangeuser() {
		return divnchangeuser;
	}

	public void setDivnchangeuser(String divnchangeuser) {
		this.divnchangeuser = divnchangeuser;
	}*/
	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
}
}

