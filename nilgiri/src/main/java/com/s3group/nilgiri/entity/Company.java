package com.s3group.nilgiri.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="COMPANY")
public class Company implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	@Column(name="COMPANY_ID")
	private Long companyId;
	
	@Column(name="COMP_COMPANY")
	private String compcompany;
	
	@Column(name="COMP_DESC")
	private String compdesc;
	
	@Column(name="COMP_ADDRESS1")
	private String compaddress1;
	
	@Column(name="COMP_ADDRESS2")
	private String compaddress2;
	
	@Column(name="COMP_CITY")
	private String compcity;
	
	@Column(name="COMP_STATE")
	private String compstate;
	
	@Column(name="COMP_ZIP")
	private String compzip;
	
	@Column(name="COMP_COUNTRY")
	private String compcountry;
	
	@Column(name="COMP_CONTACT_NAME")
	private String compcontactname;
	
	@Column(name="COMP_CONTACT_NUMBER")
	private String compcontactnumber;
	
	/*@Column(name="COMP_CREATE_TIMESTAMP")
	private String compcreatetimestamp;
	
	@Column(name="COMP_CHANGE_TIMESTAMP")
	private String compchangetimestamp;
	
	@Column(name="COMP_CREATE_USER")
	private String compcreateuser;
	
	@Column(name="COMP_CHANGE_USER")
	private String compchangeuser;*/
	
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompcompany() {
		return compcompany;
	}

	public void setCompcompany(String compcompany) {
		this.compcompany = compcompany;
	}
	
	public String getCompdesc() {
		return compdesc;
	}

	public void setCompdesc(String compdesc) {
		this.compdesc = compdesc;
	}

	public String getCompaddress1() {
		return compaddress1;
	}

	public void setCompaddress1(String compaddress1) {
		this.compaddress1 = compaddress1;
	}
	
	public String getCompanyaddress2() {
		return compaddress2;
	}

	public void setCompaddress2(String compaddress2) {
		this.compaddress2 = compaddress2;
	}
	
	public String getCompcity() {
		return compcity;
	}

	public void setCompcity(String compcity) {
		this.compcity = compcity;
	}
	
	public String getCompstate() {
		return compstate;
	}

	public void setCompstate(String compstate) {
		this.compstate = compstate;
	}
	
	public String getCompzip() {
		return compzip;
	}

	public void setCompzip(String compzip) {
		this.compzip = compzip;
	}
	
	public String getCompcountry() {
		return compcountry;
	}

	public void setCompcountry(String compcountry) {
		this.compcountry = compcountry;
	}
	
	public String getCompcontactname() {
		return compcontactname;
	}

	public void setCompcontactname(String compcontactname) {
		this.compcontactname = compcontactname;
	}
	
	public String getCompcontactnumber() {
		return compcontactnumber;
	}

	public void setCompcontactnumber(String compcontactnumber) {
		this.compcontactnumber = compcontactnumber;
	}
	
	/*public String getCompcreatetimestamp() {
		return compcreatetimestamp;
	}

	public void setCompcreatetimestamp(String compcreatetimestamp) {
		this.compcreatetimestamp = compcreatetimestamp;
	}
	
	public String getCompchangetimestamp() {
		return compchangetimestamp;
	}

	public void setCompchangetimestamp(String compchangetimestamp) {
		this.compchangetimestamp = compchangetimestamp;
	}
	
	public String getCompcreateuser() {
		return compcreateuser;
	}

	public void setCompcreateuser(String compcreateuser) {
		this.compcreateuser = compcreateuser;
	}
	
	public String getCompchangeuser() {
		return compchangeuser;
	}

	public void setCompchangeuser(String compchangeuser) {
		this.compchangeuser = compchangeuser;
	}*/
	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
}
}


